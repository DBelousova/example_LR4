﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;
using ViewModel.View.ViewModel;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using System.Runtime.CompilerServices;
using DataAccess;
using ViewModel.Annotations;


namespace ViewModel
{
    public class MainWindowViewModel: INotifyPropertyChanged
    {
        private string path;
        private IRepository Repository;
        public ObservableCollection<Student> Students { get; set; }

      

        public MainWindowViewModel()
        {
            //path = ConfigurationManager.AppSettings["LogFilePath"];
            Repository = new StudentRepository();
            Students = new ObservableCollection<Student>(Repository.GetAllStudents());

        }

        public Student SelectedStudent
        {
            get { return _selectedStudent; }
            set { _selectedStudent = value;
               OnPropertyChanged(nameof(SelectedStudent));
            }
        }

        private Student _Student;
        public Student NewStudent { get { return _Student ?? (_Student = new Student()); } set { _Student = value; } }

        private RelayCommand _addCommand;
      
        private bool _isSelected;
        private Student _selectedStudent;
        public RelayCommand AddCommand { get { return _addCommand ?? (_addCommand = new RelayCommand(Add)); } }

        private void Add(object o)
        {
            Repository.Add(NewStudent);
            Students.Add(NewStudent);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
