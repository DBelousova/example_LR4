﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Student
    {
        public int Id { get; set; }
        public string Second_name { get; set; }
        public string First_name { get; set; }
        public string Group { get; set; }
        public Teacher Teacher { get; set; }

        public override string ToString()
        {
            return Second_name;
        }
    } 
}
